package com.sevixoo.onboarding.ui.summary

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.sevixoo.onboarding.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SummaryFragment : Fragment() {

    private val viewModel: SummaryViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.summary_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        observeNavigationEvents()
        val nextButton: Button = view.findViewById(R.id.buttonNext)
        nextButton.setOnClickListener { viewModel.onClickNext() }
    }

    private fun observeNavigationEvents() {
        viewModel.events.observe(viewLifecycleOwner, Observer { event ->
            when (event) {
                is SummaryEvents.NavigateToNextScreen -> {
                    findNavController().navigate(R.id.action_summaryFragment_to_onboardingSettingsActivity)
                }
            }
        })
    }
}