package com.sevixoo.onboarding.ui.summary

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SummaryViewModel @Inject constructor() : ViewModel() {

    private val _events: MutableLiveData<SummaryEvents> = MutableLiveData()
    val events: LiveData<SummaryEvents> = _events

    fun onClickNext() {
        _events.postValue(SummaryEvents.NavigateToNextScreen)
    }
}

sealed class SummaryEvents {
    object NavigateToNextScreen : SummaryEvents()
}