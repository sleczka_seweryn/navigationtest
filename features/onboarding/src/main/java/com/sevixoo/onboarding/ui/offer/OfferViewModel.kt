package com.sevixoo.onboarding.ui.offer

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class OfferViewModel @Inject constructor() : ViewModel() {

    private val _events: MutableLiveData<OfferEvents> = MutableLiveData()
    val events: LiveData<OfferEvents> = _events

    fun onClickDocuments() {
        _events.postValue(OfferEvents.NavigateToDocuments)
    }

    fun onClickPersonalData() {
        _events.postValue(OfferEvents.NavigateToPersonalData)
    }
}

sealed class OfferEvents {
    object NavigateToDocuments: OfferEvents()
    object NavigateToPersonalData: OfferEvents()
}