package com.sevixoo.onboarding.ui.documents

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.sevixoo.onboarding.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DocumentsFragment : Fragment() {

    private val viewModel: DocumentsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.documents_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        observeNavigationEvents()
        val nextButton: Button = view.findViewById(R.id.buttonNext)
        val settingsButton: Button = view.findViewById(R.id.buttonSettings)
        nextButton.setOnClickListener { viewModel.onClickNext() }
        settingsButton.setOnClickListener { viewModel.onClickSettings() }
    }

    private fun observeNavigationEvents() {
        viewModel.events.observe(viewLifecycleOwner, Observer { event ->
            when (event) {
                is DocumentsEvents.NavigateToNextScreen -> {
                    findNavController().navigate(R.id.action_documentsFragment_to_summaryFragment)
                }
                is DocumentsEvents.NavigateToSettings -> {
                    findNavController().navigate(R.id.action_documentsFragment_to_onboardingSettingsActivity)
                }
            }
        })
    }
}