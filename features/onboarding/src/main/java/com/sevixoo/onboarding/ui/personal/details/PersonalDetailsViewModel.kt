package com.sevixoo.onboarding.ui.personal.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sevixoo.onboarding.ui.documents.DocumentsEvents
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PersonalDetailsViewModel @Inject constructor() : ViewModel() {

    private val _events: MutableLiveData<PersonalDetailsEvents> = MutableLiveData()
    val events: LiveData<PersonalDetailsEvents> = _events

    fun onClickNext() {
        _events.postValue(PersonalDetailsEvents.NavigateToNextScreen)
    }

    fun onClickSettings() {
        _events.postValue(PersonalDetailsEvents.NavigateToSettings)
    }
}

sealed class PersonalDetailsEvents {
    object NavigateToNextScreen : PersonalDetailsEvents()
    object NavigateToSettings : PersonalDetailsEvents()
}