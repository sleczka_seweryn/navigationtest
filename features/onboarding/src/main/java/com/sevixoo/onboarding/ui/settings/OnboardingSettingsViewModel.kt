package com.sevixoo.onboarding.ui.settings

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class OnboardingSettingsViewModel @Inject constructor() : ViewModel() {
}