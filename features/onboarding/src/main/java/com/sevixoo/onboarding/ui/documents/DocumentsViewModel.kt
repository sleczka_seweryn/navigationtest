package com.sevixoo.onboarding.ui.documents

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DocumentsViewModel @Inject constructor() : ViewModel() {

    private val _events: MutableLiveData<DocumentsEvents> = MutableLiveData()
    val events: LiveData<DocumentsEvents> = _events

    fun onClickNext() {
        _events.postValue(DocumentsEvents.NavigateToNextScreen)
    }

    fun onClickSettings() {
        _events.postValue(DocumentsEvents.NavigateToSettings)
    }
}

sealed class DocumentsEvents {
    object NavigateToNextScreen : DocumentsEvents()
    object NavigateToSettings : DocumentsEvents()
}