package com.sevixoo.onboarding.ui.offer

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.sevixoo.onboarding.R
import com.sevixoo.onboarding.ui.personal.details.PersonalDetailsEvents
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OfferFragment : Fragment() {

    private val viewModel: OfferViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.offer_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        observeNavigationEvents()
        val documentsButton: Button = view.findViewById(R.id.documentsNext)
        val personalButton: Button = view.findViewById(R.id.personalNext)
        documentsButton.setOnClickListener { viewModel.onClickDocuments() }
        personalButton.setOnClickListener { viewModel.onClickPersonalData() }
    }


    private fun observeNavigationEvents() {
        viewModel.events.observe(viewLifecycleOwner, Observer { event ->
            when (event) {
                is OfferEvents.NavigateToDocuments -> {
                    findNavController().navigate(R.id.action_offerFragment_to_documentsFragment)
                }
                is OfferEvents.NavigateToPersonalData -> {
                    findNavController().navigate(R.id.action_offerFragment_to_personalDetailsFragment)
                }
            }
        })
    }
}