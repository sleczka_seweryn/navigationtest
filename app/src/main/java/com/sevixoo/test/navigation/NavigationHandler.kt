package com.sevixoo.test.navigation

import android.app.Activity
import android.content.Context
import com.sevixoo.test.ui.activation.ActivationActivity
import com.sevixoo.test.ui.dashboard.DashboardActivity

class NavigationHandler {

    fun startActivationActivity(context: Activity, param1: String) {
        with(context) {
            startActivity(
                ActivationActivity.createIntent(
                    context = context,
                    param1 = param1
                )
            )
            finish()
        }
    }

    fun startDashboardActivity(context: Activity, param1: String, param2: String) {
        with(context) {
            startActivity(
                DashboardActivity.createIntent(
                    context = context,
                    param1 = param1,
                    param2 = param2
                )
            )
            finish()
        }
    }

}