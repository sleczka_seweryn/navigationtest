package com.sevixoo.test.ui.activation.categories

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.sevixoo.test.R
import com.sevixoo.test.ui.activation.providers.ProvidersFragment

class CategoriesFragment : Fragment() {

    private val viewModel: CategoriesViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeNavigationEvents()
        if (savedInstanceState == null) {
            viewModel.initialize()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                MaterialTheme {

                        Text("CategoriesFragment")
                        Button(onClick = { viewModel.onNextClicked() }) {
                            Text("Next")
                        }

                }
            }
        }
    }

    private fun observeNavigationEvents() {
        viewModel.events.observe(this, Observer { event ->
            when (event) {
                is CategoriesEvents.NavigateToProviders -> {
                    requireActivity().supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.container, ProvidersFragment.newInstance())
                        .addToBackStack(null)
                        .commit()
                }
            }
        })
    }

    companion object {
        fun newInstance() = CategoriesFragment()
    }
}