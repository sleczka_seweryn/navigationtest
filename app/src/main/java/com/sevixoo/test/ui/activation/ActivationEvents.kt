package com.sevixoo.test.ui.activation

sealed class ActivationEvents {
    object NavigateToCategories: ActivationEvents()
}