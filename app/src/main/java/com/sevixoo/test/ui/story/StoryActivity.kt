package com.sevixoo.test.ui.story

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.lifecycle.Observer
import com.sevixoo.test.core.StartActivityIntentFactory
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class StoryActivity : AppCompatActivity() {

    private val viewModel: StoryViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeNavigationEvents()
        if (savedInstanceState == null) {
            viewModel.initialize()
        }
        setContent {
            MaterialTheme {
                Scaffold {

                        Text("Story")
                        Button(onClick = { viewModel.onBackClicked() }) {
                            Text("Back")
                        }
                        Button(onClick = { viewModel.onNextClicked() }) {
                            Text("Next")
                        }

                }
            }
        }
    }

    private fun observeNavigationEvents() {
        viewModel.events.observe(this, Observer { event ->
            when (event) {
                is StoryEvents.NavigateNext -> startActivity(
                    event.intentFactory.createIntent(
                        baseContext
                    )
                )
                is StoryEvents.NavigateBack -> finish()
            }
        })
    }

    override fun onBackPressed() {
        viewModel.onBackClicked()
    }

    companion object {

        const val ARG_REDIRECT_INTENT = "redirect_intent"

        fun createIntent(context: Context, redirectIntentFactory: StartActivityIntentFactory) =
            Intent(context, StoryActivity::class.java).apply {
                putExtra(ARG_REDIRECT_INTENT, redirectIntentFactory)
            }

    }
}