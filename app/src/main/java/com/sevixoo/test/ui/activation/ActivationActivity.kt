package com.sevixoo.test.ui.activation

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.sevixoo.test.R
import com.sevixoo.test.ui.activation.categories.CategoriesFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ActivationActivity : AppCompatActivity() {

    private val viewModel: ActivationViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeNavigationEvents()
        if(savedInstanceState == null) {
            viewModel.initialize()
        }
        setContentView(R.layout.main_activity)
    }

    private fun observeNavigationEvents() {
        viewModel.events.observe(this, Observer { event ->
            when(event) {
                is ActivationEvents.NavigateToCategories -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.container, CategoriesFragment.newInstance())
                        .commit()
                }
            }
        })
    }

    companion object {

        const val ARG_PARAM_1 = "param1"

        fun createIntent(context: Context, param1: String) = Intent(context, ActivationActivity::class.java).apply {
            putExtra(ARG_PARAM_1, param1)
        }
    }
}