package com.sevixoo.test.ui.activation.providers

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProvidersViewModel @Inject constructor(

) : ViewModel() {

    private val _events: MutableLiveData<ProvidersEvents> = MutableLiveData()
    val events: LiveData<ProvidersEvents> = _events

    fun initialize() {
        //TODO
    }

    fun onNextClicked() {
        _events.postValue(ProvidersEvents.NavigateToSummary(
            param1 = "A",
            param2 = "B"
        ))
    }
}