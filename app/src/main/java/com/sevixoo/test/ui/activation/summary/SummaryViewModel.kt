package com.sevixoo.test.ui.activation.summary

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SummaryViewModel @Inject constructor(

) : ViewModel() {

    private val _events: MutableLiveData<SummaryEvents> = MutableLiveData()
    val events: LiveData<SummaryEvents> = _events

    fun initialize() {
        //TODO
    }

    fun onNextClicked() {
        _events.postValue(
            SummaryEvents.NavigateToDashboard(
                param1 = "A",
                param2 = "B"
            )
        )
    }

}