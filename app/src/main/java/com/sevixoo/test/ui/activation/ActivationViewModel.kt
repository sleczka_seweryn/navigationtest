package com.sevixoo.test.ui.activation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sevixoo.test.ui.initialize.InitializeEvents
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ActivationViewModel @Inject constructor(

) : ViewModel() {

    private val _events: MutableLiveData<ActivationEvents> = MutableLiveData()
    val events: LiveData<ActivationEvents> = _events

    fun initialize() {
        _events.postValue(ActivationEvents.NavigateToCategories)
    }
}