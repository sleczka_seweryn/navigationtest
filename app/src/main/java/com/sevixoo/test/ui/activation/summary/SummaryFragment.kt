package com.sevixoo.test.ui.activation.summary

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.sevixoo.test.ui.dashboard.DashboardActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SummaryFragment : Fragment() {

    private val viewModel: SummaryViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeNavigationEvents()
        if (savedInstanceState == null) {
            viewModel.initialize()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {

                    Text("SummaryFragment")
                    Button(onClick = { viewModel.onNextClicked() }) {
                        Text("Next")
                    }

            }
        }
    }

    private fun observeNavigationEvents() {
        viewModel.events.observe(this, Observer { event ->
            when (event) {
                is SummaryEvents.NavigateToDashboard -> {
                    with(requireActivity()) {
                        startActivity(
                            DashboardActivity.createIntent(
                                context = requireActivity(),
                                param1 = event.param1,
                                param2 = event.param2
                            )
                        )
                        finish()
                    }
                }
            }
        })
    }

    companion object {
        fun newInstance() = SummaryFragment()
    }
}