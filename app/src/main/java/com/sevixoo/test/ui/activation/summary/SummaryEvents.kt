package com.sevixoo.test.ui.activation.summary

sealed class SummaryEvents {
    data class NavigateToDashboard(
        val param1: String,
        val param2: String
    ): SummaryEvents()
}