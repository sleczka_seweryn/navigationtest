package com.sevixoo.test.ui.initialize

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.lifecycle.Observer
import com.sevixoo.test.navigation.NavigationHandler
import com.sevixoo.test.ui.story.StoryActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class InitializeActivity : AppCompatActivity() {

    private val viewModel: InitializeViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeNavigationEvents()
        if (savedInstanceState == null) {
            viewModel.initialize()
        }
        setContent {
            MaterialTheme {
                Scaffold {
                    CircularProgressIndicator()
                }
            }
        }
    }

    private fun observeNavigationEvents() {
        viewModel.events.observe(this, Observer { event ->
            when (event) {
                is InitializeEvents.NavigateToMain -> {
                    NavigationHandler().startDashboardActivity(
                        this,
                        event.param1,
                        event.param2
                    )
                }
                is InitializeEvents.NavigateToActivation -> {
                    NavigationHandler().startActivationActivity(this, event.param1)
                }
                is InitializeEvents.NavigateToStory -> {
                    startActivity(
                        StoryActivity.createIntent(
                            baseContext,
                            ActivationRedirectIntentFactory(param1 = event.param1)
                        )
                    )
                    finish()
                }
            }
        })
    }
}