package com.sevixoo.test.ui.activation.providers

sealed class ProvidersEvents {
    data class NavigateToSummary(
        val param1: String,
        val param2: String
    ): ProvidersEvents()
}