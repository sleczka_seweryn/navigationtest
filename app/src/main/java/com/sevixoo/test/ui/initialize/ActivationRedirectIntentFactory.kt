package com.sevixoo.test.ui.initialize

import android.content.Context
import com.sevixoo.test.core.StartActivityIntentFactory
import com.sevixoo.test.ui.activation.ActivationActivity
import kotlinx.parcelize.Parcelize

@Parcelize
class ActivationRedirectIntentFactory(
    private val param1: String
) : StartActivityIntentFactory {
    override fun createIntent(context: Context) = ActivationActivity.createIntent(context, param1)
}