package com.sevixoo.test.ui.credit

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import com.sevixoo.test.R
import com.sevixoo.test.ui.dashboard.DashboardActivity
import com.sevixoo.test.ui.dashboard.DashboardViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CreditActivity : AppCompatActivity() {

    private val viewModel: DashboardViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            viewModel.initialize()
        }
        setContent {
            MaterialTheme {
                Scaffold {

                }
            }
        }
    }

    companion object {

        const val ARG_PARAM_1 = "param1"
        const val ARG_PARAM_2 = "param2"

        fun createIntent(context: Context, param1: String, param2: String) =
            Intent(context, DashboardActivity::class.java).apply {
                putExtra(ARG_PARAM_1, param1)
                putExtra(ARG_PARAM_2, param2)
            }
    }
}