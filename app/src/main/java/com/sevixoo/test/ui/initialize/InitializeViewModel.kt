package com.sevixoo.test.ui.initialize

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class InitializeViewModel @Inject constructor() : ViewModel() {

    private val _events: MutableLiveData<InitializeEvents> = MutableLiveData()
    val events: LiveData<InitializeEvents> = _events

    fun initialize() {
        val isUserActive = false
        if(isUserActive) {
            _events.postValue(InitializeEvents.NavigateToActivation(
                param1 = "A"
            ))
        } else {
            _events.postValue(InitializeEvents.NavigateToMain(
                param1 = "A",
                param2 = "B"
            ))
        }
    }
}