package com.sevixoo.test.ui.story

import com.sevixoo.test.core.StartActivityIntentFactory

sealed class StoryEvents {
    object NavigateBack : StoryEvents()
    data class NavigateNext(
        val intentFactory: StartActivityIntentFactory
    ) : StoryEvents()
}