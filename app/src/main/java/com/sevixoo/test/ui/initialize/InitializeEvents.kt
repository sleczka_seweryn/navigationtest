package com.sevixoo.test.ui.initialize

sealed class InitializeEvents {

    data class NavigateToActivation(
        val param1: String
    ): InitializeEvents()

    data class NavigateToMain(
        val param1: String,
        val param2: String
    ): InitializeEvents()

    data class NavigateToStory(
        val param1: String = "D"
    ): InitializeEvents()
}