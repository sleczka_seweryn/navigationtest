package com.sevixoo.test.ui.activation.categories

sealed class CategoriesEvents {
    object NavigateToProviders: CategoriesEvents()
}