package com.sevixoo.test.ui.dashboard

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.sevixoo.test.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DashboardActivity : AppCompatActivity() {

    private val viewModel: DashboardViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(savedInstanceState == null) {
            viewModel.initialize()
        }
        setContentView(R.layout.main_activity)
    }

    companion object {

        const val ARG_PARAM_1 = "param1"
        const val ARG_PARAM_2 = "param2"

        fun createIntent(context: Context, param1: String, param2: String) =
            Intent(context, DashboardActivity::class.java).apply {
                putExtra(ARG_PARAM_1, param1)
                putExtra(ARG_PARAM_2, param2)
            }
    }
}