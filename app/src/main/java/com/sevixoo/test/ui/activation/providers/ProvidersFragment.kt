package com.sevixoo.test.ui.activation.providers

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.sevixoo.test.R
import com.sevixoo.test.ui.activation.ActivationEvents
import com.sevixoo.test.ui.activation.categories.CategoriesFragment
import com.sevixoo.test.ui.activation.categories.CategoriesViewModel
import com.sevixoo.test.ui.activation.summary.SummaryFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProvidersFragment : Fragment() {

    private val viewModel: ProvidersViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeNavigationEvents()
        if (savedInstanceState == null) {
            viewModel.initialize()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {

                    Text("ProvidersFragment")
                    Button(onClick = { viewModel.onNextClicked() }) {
                        Text("Next")
                    }

            }
        }
    }

    private fun observeNavigationEvents() {
        viewModel.events.observe(this, Observer { event ->
            when (event) {
                is ProvidersEvents.NavigateToSummary -> {
                    requireActivity().supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.container, SummaryFragment.newInstance())
                        .addToBackStack(null)
                        .commit()
                }
            }
        })
    }

    companion object {
        fun newInstance() = ProvidersFragment()
    }
}