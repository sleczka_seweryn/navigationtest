package com.sevixoo.test.ui.activation.categories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sevixoo.test.ui.initialize.InitializeEvents
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CategoriesViewModel @Inject constructor(

) : ViewModel() {

    private val _events: MutableLiveData<CategoriesEvents> = MutableLiveData()
    val events: LiveData<CategoriesEvents> = _events

    fun initialize() {
        //TODO
    }

    fun onNextClicked() {
        _events.postValue(CategoriesEvents.NavigateToProviders)
    }
}