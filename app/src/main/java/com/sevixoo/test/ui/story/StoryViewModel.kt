package com.sevixoo.test.ui.story

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.sevixoo.test.core.StartActivityIntentFactory
import dagger.hilt.android.lifecycle.HiltViewModel
import java.lang.IllegalStateException
import javax.inject.Inject

@HiltViewModel
class StoryViewModel @Inject constructor(
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val redirectIntentFactory: StartActivityIntentFactory =
        savedStateHandle[StoryActivity.ARG_REDIRECT_INTENT]
            ?: throw IllegalStateException("Arg not found")

    private val _events: MutableLiveData<StoryEvents> = MutableLiveData()
    val events: LiveData<StoryEvents> = _events

    fun initialize() {
        //TODO
    }

    fun onNextClicked() {
        _events.postValue(StoryEvents.NavigateNext(redirectIntentFactory))
    }

    fun onBackClicked() {
        _events.postValue(StoryEvents.NavigateBack)
    }
}