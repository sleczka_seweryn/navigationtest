package com.sevixoo.test.core

import android.content.Context
import android.content.Intent
import android.os.Parcelable

interface StartActivityIntentFactory : Parcelable {
    fun createIntent(context: Context): Intent
}